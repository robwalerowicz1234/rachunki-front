import * as React from "react"
import { storiesOf } from "@storybook/react"
import Registration from "./Registration2"

storiesOf("Registration2", module)
    .add("with basic log function", () => {
        const clickFunction = () => console.log('You clicked custom function')
        return <Registration onAccept={clickFunction} />
    })