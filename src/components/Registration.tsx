import * as React from 'react'
import { useState, Fragment } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import { Subject, timer, Subscription } from 'rxjs'
import { debounce, distinctUntilChanged } from 'rxjs/operators'
import * as RValidators from '../validators/RegistrationValidator'

export interface IRegistration {
    onAccept?: (e: any) => void
}

export interface IRegistrationErrors {
    login: string,
    email: string,
    password: string,
    passwordConfirmation: string
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
        boxShadow: '0 3px 5px 2px rgba(200, 200, 200, .3)',
        display: 'grid',
        gridTemplateColumns: '1fr 7fr 1fr',
        gridTemplateRows: '1fr 9fr 1fr',
        height: '70vh',
        width: '30vw'
    },
    registrationHeader: {
        gridColumn: '2 / 3',
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        gridColumn: '2 / 3',
        gridRow : '2 / 3',
        width: '100%',
        height: '100%'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
  }),
);

// Login states
const loginSubject = new Subject<string>()
const debouncedLogin = loginSubject.pipe(
    debounce(() => timer(1000)),
    distinctUntilChanged()
)
let loginSubscription: Subscription = Subscription.EMPTY;


// Email states
const emailSubject = new Subject<string>()
const debouncedEmail = emailSubject.pipe(
    debounce(() => timer(1000)),
    distinctUntilChanged()
)
let emailSubscription: Subscription = Subscription.EMPTY;

// Password states
const passwordSubject = new Subject<string>()
const debouncedPassword = passwordSubject.pipe(
    debounce(() => timer(1000)),
    distinctUntilChanged()
)
let passwordSubscription: Subscription = Subscription.EMPTY;

// Password confirmation states
const passwordConfirmationSubject = new Subject<string>()
const debouncedPasswordConfirmation = passwordConfirmationSubject.pipe(
    debounce(() => timer(1000)),
    distinctUntilChanged()
)
let passwordConfirmationSubscription: Subscription = Subscription.EMPTY;


const Registration: React.FunctionComponent<IRegistration> = (props) => {
    const classes = useStyles();
    const [login, setLogin] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [passwordConfirmation, setPasswordConfirmation] = useState<string>('')

    const [errors, setErrors] = useState<IRegistrationErrors>({login: '', email: '', password: '', passwordConfirmation: ''})

    // Login handlers
    const setLoginErrors = (value: string) => {
        const loginErrors = RValidators.validateLogin(value)
        if(loginErrors.length !== 0) {
            setErrors({ ...errors, login: loginErrors.join(', ') })
        } else {
            setErrors({ ...errors, login: '' })
        }
    }

    const onChangeLogin = (ev: React.ChangeEvent<HTMLInputElement>): void => {
        const e = ev.target.value
        loginSubject.next(e)
        setLogin(e)
    }

    const onFocusLogin = () => {
        loginSubscription = debouncedLogin.subscribe(setLoginErrors)
    }

    const onBlurLogin = (value: React.FocusEvent<HTMLInputElement>) => {
        loginSubscription.unsubscribe()
        setLoginErrors(value.target.value)
    }

    // Email handlers
    const setEmailErrors = (value: string) => {
        const emailErrors = RValidators.validateEmail(value)
        if(emailErrors.length !== 0) {
            setErrors({ ...errors, email: emailErrors.join(', ') })
        } else {
            setErrors({ ...errors, email: '' })
        }
    }

    const onChangeEmail = (ev: React.ChangeEvent<HTMLInputElement>): void => {
        const e = ev.target.value
        emailSubject.next(e)
        setEmail(e)
    }

    const onFocusEmail = () => {
        emailSubscription = debouncedEmail.subscribe(setEmailErrors)
    }

    const onBlurEmail = (value: React.FocusEvent<HTMLInputElement>) => {
        emailSubscription.unsubscribe()
        setEmailErrors(value.target.value)
    }

    // Password handlers
    const setPasswordErrors = (value: string) => {
        const passwordErrors = RValidators.validatePassword(value)
        if(passwordErrors.length !== 0) {
            setErrors({ ...errors, password: passwordErrors.join(', ') })
        } else {
            setErrors({ ...errors, password: ''})
        }
    }

    const onChangePassword = (ev: React.ChangeEvent<HTMLInputElement>): void => {
        const e = ev.target.value
        passwordSubject.next(e)
        setPassword(e)
    }

    const onFocusPassword = () => {
        passwordSubscription = debouncedPassword.subscribe(setPasswordErrors)
    }

    const onBlurPassword = (value: React.FocusEvent<HTMLInputElement>) => {
        passwordSubscription.unsubscribe()
        setPasswordErrors(value.target.value)
    }

    // Password Confirmation handlers
    const setPasswordConfirmationErrors = (value: string) => {
        const passwordConfirmationErrors = RValidators.validateConfirmedPassword(password, value)
        if(passwordConfirmationErrors.length !== 0) {
            setErrors({ ...errors, passwordConfirmation: passwordConfirmationErrors.join(', ')})
        } else {
            setErrors({ ...errors, passwordConfirmation: '' })
        }
    }

    const onChangePasswordConfirmation = (ev: React.ChangeEvent<HTMLInputElement>): void => {
        const e = ev.target.value
        passwordConfirmationSubject.next(e)
        setPasswordConfirmation(e)
    }

    const onFocusPasswordConfirmation = () => {
        passwordConfirmationSubscription = debouncedPasswordConfirmation.subscribe(setPasswordConfirmationErrors)
    }

    const onBlurPasswordConfirmation = (value: React.FocusEvent<HTMLInputElement>) => {
        passwordConfirmationSubscription.unsubscribe()
        setPasswordConfirmationErrors(value.target.value)
    }

    const submitUserRegistration = (e: any): void => {  
        console.log('submitting form') 
    }  

    return (
        <Fragment>
            <div className={classes.container}>
                <h3 className={classes.registrationHeader} >Registration page</h3>
                <form className={classes.form} method="post" name="userRegistrationForm" onSubmit={submitUserRegistration}>
                    <TextField
                        id="login-text"
                        label="Login"
                        className={classes.textField}
                        value={login}
                        onChange={onChangeLogin}
                        margin="normal"
                        error={errors.login !== ''}
                        helperText={errors.login}
                        onFocus={onFocusLogin}
                        onBlur={onBlurLogin}
                        required={true}
                    />
                    <TextField
                        id="email-text"
                        label="Email"
                        className={classes.textField}
                        value={email}
                        onChange={onChangeEmail}
                        margin="normal"
                        error={errors.email !== ''}
                        helperText={errors.email}
                        onFocus={onFocusEmail}
                        onBlur={onBlurEmail}
                        required={true}
                    />
                    <TextField 
                        id="password-text"
                        label="Password"
                        className={classes.textField}
                        value={password}
                        onChange={onChangePassword}
                        margin="normal"
                        type="password"
                        error={errors.password !== ''}
                        helperText={errors.password}
                        onFocus={onFocusPassword}
                        onBlur={onBlurPassword}
                        required={true}
                    />

                    <TextField 
                        id="password-confirmation-text"
                        label="Confirm password"
                        className={classes.textField}
                        value={passwordConfirmation}
                        onChange={onChangePasswordConfirmation}
                        margin="normal"
                        type="password"
                        error={errors.passwordConfirmation !== ''}
                        helperText={errors.passwordConfirmation}
                        onFocus={onFocusPasswordConfirmation}
                        onBlur={onBlurPasswordConfirmation}
                        required={true}
                    />
                </form>
            </div>
        </Fragment>
    )
}

export default Registration;