import Registration from "./Registration"
import * as React from "react"
import { storiesOf } from "@storybook/react"

storiesOf("Registration", module)
    .add("with basic log function", () => {
        const clickFunction = () => console.log('You clicked custom function')
        return <Registration onAccept={clickFunction} />
    })