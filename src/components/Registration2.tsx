import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { TextField, Button } from '@material-ui/core'
import { TextFieldProps } from '@material-ui/core/TextField/TextField'
import { Field, FieldProps, Form, Formik, FormikProps } from 'formik'
import * as React from 'react'
import { RegistrationFormSchemaValidator } from '../validators/RegistrationValidator'

export interface IRegistration {
    onAccept?: (e: any) => void
}

export interface IRegistrationFormSchema {
    login: string,
    email: string,
    password: string,
    passwordConfirmation: string
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
        display: 'grid',
        gridTemplateColumns: '1fr 7fr 1fr',
        gridTemplateRows: '1fr 9fr 1fr',
        boxShadow: '0 3px 5px 2px rgba(200, 200, 200, .3)',
        width: 'wrap-content',
        height: 'wrap-content',
        maxWidth: '400px',
        maxHeight: '500px'
    },
    registrationHeader: {
        gridColumn: '2 / 3',
        textAlign: 'center'
    },
    form: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        gridColumn: '2 / 3',
        gridRow : '2 / 3',
        width: '100%',
        height: '100%'
    },
    submitButton: {
        marginTop: '3em'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1)
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
  }),
);

const MaterialFormikTextField: React.FC<FieldProps & TextFieldProps> = (props) => {
    const { type, margin, className, label, placeholder, error, helperText, field } = props
    return <TextField 
        margin={margin} 
        className={className} 
        helperText={helperText}
        placeholder={placeholder}  
        error={error}
        label={label}
        type={type}
        {...field} />
}

const initialValues: IRegistrationFormSchema = {
    login: '',
    email: '',
    password: '',
    passwordConfirmation: ''
}

const RenderRegistration: (props: FormikProps<IRegistrationFormSchema>) => React.ReactNode = ({errors, touched }) => {
    const classes = useStyles()
    return (
        <div className={classes.container}>
            <h3 className={classes.registrationHeader}>Registration page</h3>
            <Form className={classes.form} method="post" name="userRegistrationForm">
                <Field
                    name="login"
                    label="Login"
                    className={classes.textField}
                    margin="normal"
                    component={MaterialFormikTextField}
                    error={errors.login && touched.login}
                    helperText={errors.login && touched.login ? errors.login : ''}
                    required={true}
                />
                <Field
                    name="email"
                    label="Email"
                    className={classes.textField}
                    margin="normal"
                    component={MaterialFormikTextField}
                    error={errors.email && touched.email}
                    helperText={errors.email && touched.email ? errors.email : ''}
                    required={true}
                />
                <Field 
                    name="password"
                    label="Password"
                    className={classes.textField}
                    margin="normal"
                    type="password"
                    component={MaterialFormikTextField}
                    error={errors.password && touched.password}
                    helperText={errors.password && touched.password ? errors.password : ''}
                    required={true}
                />
                <Field 
                    name="passwordConfirmation"
                    label="Confirm password"
                    className={classes.textField}
                    margin="normal"
                    type="password"
                    component={MaterialFormikTextField}
                    error={errors.passwordConfirmation && touched.passwordConfirmation}
                    helperText={errors.passwordConfirmation && touched.passwordConfirmation ? errors.passwordConfirmation: ''}
                    required={true}
                />
                <Button variant="contained" color="primary" className={classes.submitButton}>Submit</Button>
            </Form>
        </div>
    )
}

const Registration: React.FunctionComponent<IRegistration> = (props) => {
    const onSubmit = (values: IRegistrationFormSchema) => {
        return props.onAccept && props.onAccept(values)
    }
    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            render={RenderRegistration}
            validationSchema={RegistrationFormSchemaValidator}
        />
    )
}

export default Registration;