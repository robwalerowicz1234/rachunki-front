import * as Yup from 'yup'

export function validateLogin(login: string): string[] {

    if (!login) {
        return ['Login should not be empty']
    }

    const errors = []

    if (login.length <= 3) {
        errors.push('Login is to short')
    }

    return errors
}

export function validateEmail(email: string): string[] {

    if (!email) {
        return ['Email should not be empty']
    }

    const errors = []

    const emialRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if(!emialRegex.test(email.toLocaleLowerCase())) {
        errors.push('Given email is invalid')
    }

    return errors
}

export function validatePassword(password: string): string[] {

    if (!password) {
        return ['Password should not be empty']
    }

    const errors = []

    const specialChars = ['.', ',', '[', ']', '-', '!', '?', '<', '>', '(', ')']
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    if(!specialChars.some(x => password.includes(x))) {
        errors.push('Password should contain at least one special character')
    } 
    if(!numbers.some(x => password.includes(x))) {
        errors.push('Password should contain at least one number')
    }

    return errors
}

export function validateConfirmedPassword(password: string, passwordConfirmation: string): string[] {

    if (!passwordConfirmation) {
        return ['Password confirmation should not be empty']
    }

    const errors = []

    if(password !== passwordConfirmation) {
        errors.push('Passwords do not match')
    }

    return errors
}

export const RegistrationFormSchemaValidator: any = Yup.object().shape({
    login: Yup.string()
        .min(4, 'Login is to short')
        .required('Login is required'),
    email: Yup.string()
        .email('Invalid email form')
        .required('Email is required'),
    password: Yup.string()
        .matches(/\,|\.|\/|\[|\]|\-/, 'Password should contain one of characters [,./[]-]')
        .matches(/1|2|3|4|5|6|7|8|9|0/, 'Password should contain a number')
        .required('Password is required'),
    passwordConfirmation: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Passwords are not the same')
        .required('Password confirmation is required')
});